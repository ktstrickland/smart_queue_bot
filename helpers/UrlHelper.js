/*jshint strict:false */
/*jshint esversion: 6 */

const UrlHelper = {

    getDomainUrl: (account, service) => {
        return "http://api.liveperson.net/api/account/"+account+"/service/"+service+"/baseURI.json?version=1.0";   
    },

    getLoginUrl: (domain, account) => {
       return  "https://"+domain+"/api/account/"+account+"/login?v=1.3";
    },

    getAgentSummary: (domain, account) =>{

        return "https://"+domain+"/messaging_history/api/account/"+account+"/agent-view/summary";
    },

    getQueueHealth: (domain, account) =>{

       return "https://"+domain+"/operations/api/account/"+account+"/msgconversation";
    },

    getSkillList: (domain, account) =>{

       return "https://"+domain+"/api/account/"+account+"/configuration/le-users/skills";
    },
    
    getAgent: (domain, account, agentId) =>{

        return "https://"+domain+"/api/account/"+account+"/configuration/le-users/users/"+agentId+"?v=4.0";
    },

    getMsgInteractions: (domain, account) =>{

       return "https://"+domain+"/messaging_history/api/account/"+account+"/conversations/search?offset=0&limit=100";
    }
}


module.exports = {
    UrlHelper: UrlHelper
}
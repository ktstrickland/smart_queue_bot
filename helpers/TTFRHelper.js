/*jshint strict:false */
/*jshint esversion: 6 */

const express = require('express');
const rp = require('request-promise');
const uHelper = require('../helpers/UrlHelper').UrlHelper;
const config = require('../resources/config');

var options, domain, overflowId, convos, msgs, isHuman;
var dSkills, dMsgInt, dAgents;
var convoStartL, msgStartL, hCount=0, hTime=0, avgHTTFR=-1;

// create timestamps in ms
var searchMin = 15;
var convoStartTo = Math.round((new Date()).getTime());;
var convoStartFrom = convoStartTo - (searchMin*60*1000);
console.log('from: '+convoStartFrom,'to: '+convoStartTo);

function promise1() { // get skill domain
    return rp({url:uHelper.getDomainUrl(config.account, config.acctCfg)})
    .then((body) => {
        dSkills = JSON.parse(body).baseURI;
        console.log('skill domain: '+dSkills);
        return Promise.resolve(dSkills);
    })
    .catch(function(err){
        console.log(err);
    });
}
function promise2() { // get Messaging Interactions domain
    return rp({url:uHelper.getDomainUrl(config.account, config.msgHist)})
    .then((body) => {
        dMsgInt = JSON.parse(body).baseURI;
        console.log('msgint domain: '+dMsgInt);
        return Promise.resolve(dMsgInt);
    })
    .catch(function(err){
        console.log(err);
    });    
}
function promise3() { // get agent domain
    return rp({url:uHelper.getDomainUrl(config.account, config.acctCfg)})
    .then((body) => {
        dAgents = JSON.parse(body).baseURI;
        console.log('agent domain: '+dAgents);
        return Promise.resolve(dAgents);
    })
    .catch(function(err){
        console.log(err);
    });
}
function promise4() { // get overflow skill id
    return rp({
        url:uHelper.getSkillList(dSkills, config.account),
        oauth: config.oauth,
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then((body) => {
        var skills = JSON.parse(body);
        var as=skills.filter(function (i,n){
            return skills[n].name.toLowerCase().search(/messaging\_overflow/i)>-1;
        });
        overflowId = '';
        for (var i=0;i<as.length;i++){
            if (i>0){ overflowId += ',';}
            overflowId += as[i].id;
        }
        console.log('overflow id: '+overflowId);       
        return Promise.resolve(overflowId);
    })
    .catch(function(err){
        console.log(err);
    });
}
function promise5() { // get conversation history
    return rp({
        url:uHelper.getMsgInteractions(dMsgInt, config.account),
        oauth: config.oauth,
        method: "post",
        json: {"start":{"from":convoStartFrom,"to":convoStartFrom},"skillIds":[overflowId]},
        headers: {'Content-Type': 'application/json'}
    })
    .then((body) => {
        convos = body.conversationHistoryRecords;
        if (convos.length==0) {
            return Promise.reject('No conversations returned.');
        } else {
            console.log('conversations: '+convos.length);
            return Promise.resolve(convos);
        }
    })
    .catch(function(err){
        console.log(err);
    });
}
function promise6(msg,httfr) { // get human users
    options = {
        url:uHelper.getAgent(dAgents, config.account, msg.participantId),
        oauth: config.oauth,
        headers: {'Content-Type': 'application/json'}
    };
    return rp(options)
        .then((body) => {
            body = JSON.parse(body);
            if (body.userTypeId==1){
                isHuman = true;
                hTime += httfr;
                hCount += 1;
                avgHTTFR = (hTime/hCount).toFixed(2);
            } else {
                isHuman = false;
            }
            return Promise.resolve(avgHTTFR);
        })
        .catch(function (err) {
            console.log(err);
        }
    );   
}

// Begin promise chain
function getTTFR() { promise1() // get skill domain
.catch((err) => { console.log(err);Promise.reject(err);})
.finally(() => { 
    promise2() // get messaging interactions domain
    .catch((err) => { console.log(err);Promise.reject(err);})
    .finally(() => { 
        promise3() // get agent domain
        .catch((err) => { console.log(err);Promise.reject(err);})
        .finally(() => { 
            promise4() // get overflow skill id
            .catch((err) => { console.log(err);Promise.reject(err);})
            .finally(() => { 
                promise5() // get conversation history
                .catch((err) => { console.log(err);Promise.reject(err);})
                .finally(() => { 
                    for (var i=0;i<convos.length;i++){  
                        convoStartL = convos[i].info.startTimeL;
                        msgs = convos[i].messageRecords;
                        // console.log(convos[i].info.conversationId);
                        for (var n=0;n<msgs.length;n++){
                            var msg = msgs[n];
                            var m = 0;
                            if (msg.sentBy == 'Agent') {
                                var httfr = (msg.timeL-convoStartL)/(60*1000);
                                httfr = parseInt(httfr);
                                promise6(msg, httfr) // get human users
                                .catch((err) => {console.log(err);Promise.reject(err);})
                                .finally(() => {
                                    return;
                                });
                            }
                        }
                    }
                });
            });
        });
    });
});
}

setTimeout(function(){
    if (avgHTTFR >= 0) {
        console.log('average human TTFR: '+avgHTTFR+' min\n'); 
    } else {
        console.log('no human messages\n');
    }  
},10*1000);

module.exports = {
    getTTFR: getTTFR
};
var AppCredentials = function(credentials){
    this.username = credentials.username;
    this.appkey = credentials.appkey;
    this.secret = credentials.secret;
    this.accessToken = credentials.accessToken;
    this.accessTokenSecret = credentials.accessToken;
}
/*jshint strict:false */
/*jshint esversion: 6 */

const express = require('express');
const axios = require('axios');
const request = require('request');
const cron = require('node-cron');
const app = express();
const uHelper = require('./helpers/UrlHelper').UrlHelper;
const tHelper = require('./helpers/TTFRHelper').getTTFR;
const config = require('./resources/config');
const User = require('./model/User').User;
const OAuth   = require('oauth-1.0a');
const crypto  = require('crypto');

const ts = function(){
    return Math.round((new Date()).getTime() / 1000);
};

/**
 *  Starting Cron
 */
cron.schedule('0 */5 * * * *', () => {

    /* logs here */
    console.log(new Date());
    
    tHelper();

    /**
     * get 
     */

    /**
     *  getting skills domain
     */
    request.get({
        url:uHelper.getDomainUrl(config.account, config.acctCfg)}, 
        function(error, response, body){

            /* logs here */
            var domain = JSON.parse(body).baseURI;
            console.log('Domain: '+domain);
    
            /**
             *  getting skill id
             */
            request.get({
                url:uHelper.getSkillList(domain, config.account),
                oauth: config.oauth,
                headers: {
                    'Content-Type': 'application/json'
                }}, function(error,response,body) {

                    /* logs here */
                    var skills = JSON.parse(body);
                    // console.log(skills);

                    var as=skills.filter(function (i,n){
                        // console.log(skills[n].name);
                        return skills[n].name.toLowerCase().search(/\d{1,2}\s\(\d{6}\)/i)>-1;
                    });

                    var skillIds = '';
                    for (var i=0;i<as.length;i++){
                        if (i>0){ skillIds += ',';}
                        skillIds += as[i].id;
                        console.log(as[i].name);
                    }
                    console.log(skillIds);

                    /**
                     *  Getting data reporting domain
                     */
                    request.get({
                        url:uHelper.getDomainUrl(config.account, config.leDataReporting)}, 
                        function(error, response, body){

                            /* logs here */
                            var domain = JSON.parse(body).baseURI;
                            console.log('Domain: '+domain);

                            /**
                             *  getting
                             */
                            request.post({
                                url:uHelper.getQueueHealth(domain, config.account),
                                oauth: config.oauth,
                                json: {"timeframe":"30","skillIds":skillIds,"v":"1"},
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }, function(error,response,body) {

                                /* queue health logs here */
                                console.log(body.skillsMetricsPerAgent.metricsPerSkill);

                //                     /**
                //                      * Getting Agent Status Domain
                //                      */
                //                     axios.get(uHelper.getDomainUrl(config.account, config.msgHist))
                //                          .then( response => {

                //                             /* logs here */
                //                             let domain = response.data.baseURI;

                //                             /**
                //                              * Getting current agent status
                //                              */
                //                             axios.post(uHelper.getAgentStatus(domain, config.account),{},config.oauth)
                //                                  .then( response => {

                //                                     /* logs here */
                //                                     // console.log(response.data.summaryResults);
                                            
                //                                  })
                //                                  .catch( error =>{

                //                                     /* logs here */
                //                                     console.log(error);

                //                                  });

                //                          })
                //                          .catch( error => {

                //                             /* logs here */
                //                             console.log(error);
                //                          });
                //                  })
                //                  .catch( error =>{

                //                     /* logs here */
                //                     console.log(error);

                //                  });

                        });
                        
                    });

             });

     });
});
/**
 * 
 */
app.listen(3000, (err) =>{

    if(err){
        console.log("error starting server at port 3000");
    }else{
        console.log("server listening at port 3000");
    }
});